package TestNg;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

public class Activity_2 {
	WebDriver driver;
  
  @BeforeMethod
  public void beforeMethod() {
	  driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/lms");
  }
  
  @Test
    public void openLMSwebsite() {
  
	  String title = driver.getTitle();
  	  System.out.println("Page title is: " + title);
	  Assert.assertEquals("Learn from Industry Experts", title);
  
  }
  @AfterMethod
  public void afterMethod() {
	//driver.close();
  }

}